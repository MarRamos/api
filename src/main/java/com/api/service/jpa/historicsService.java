package com.api.service.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.api.DTO.ArticleHistoricsDTO;
import com.api.DTO.ArticlesHistoricRequestDTO;
import com.api.DTO.ArticlesHistoricResponseDTO;
import com.api.DTO.ErrorDTO;
import com.api.DTO.ErrorFieldDTO;
import com.api.entity.Articleshistorics;
import com.api.repository.historicsRepository;
import com.api.service.IhistoricsService;
@Service
public class historicsService implements IhistoricsService{
	@Autowired
	private historicsRepository historicsRepository;
	
	@Override
	public List<ArticleHistoricsDTO> ListArticleHistorics(Long articleTic, List<Integer> fieldTic) {
		
		List<ArticleHistoricsDTO> lisArticleHistoricsDTOs=new ArrayList<>();
		var listaobc = historicsRepository.findByArticleTicAndFieldTicIn( articleTic,fieldTic);
		
		 Map<Long, List<Articleshistorics>> collect1=listaobc.stream().collect(Collectors.groupingBy(Articleshistorics::getParentHistoricTic));
		collect1.forEach((key, value) -> lisArticleHistoricsDTOs.add(new ArticleHistoricsDTO(key,value)));
		
		return lisArticleHistoricsDTOs;
	}
	
	public ErrorDTO error() {
		ErrorFieldDTO errorFieldDTO = new ErrorFieldDTO();
        errorFieldDTO.setField("1");
        errorFieldDTO.setErrorFieldKey("INVALID_MQS");
        errorFieldDTO.setDescription("Inconsistency in mq parameter. Make sure you use the pattern model-quality in a consistent way");
        List<ErrorFieldDTO> listErrorFieldDTOs=new ArrayList<>();
        listErrorFieldDTOs.add(errorFieldDTO);
          return new  ErrorDTO("Error 400",listErrorFieldDTOs);
	}

	@Override
	public Page<Articleshistorics> TodasPaginacion(int pagina,int cantidad ) {
		
		return historicsRepository.findAll(PageRequest.of(pagina, cantidad));

	}

	@Override
	public List<ArticleHistoricsDTO> ListArticleHistorics(ArticlesHistoricRequestDTO articlehis,int pagina,int cantidad ) {
		Pageable pag=PageRequest.of(pagina, cantidad);
		var paginas= historicsRepository.findByArticleTicAndFieldTicIn(articlehis.getArticleTic(), articlehis.getFieldTics(),pag);
		List<ArticleHistoricsDTO> lisArticleHistoricsDTOs=new ArrayList<>();
		 Map<Long, List<Articleshistorics>> coleccion=paginas.stream().collect(Collectors.groupingBy(Articleshistorics::getParentHistoricTic));
		 coleccion.forEach((key, value) -> lisArticleHistoricsDTOs.add(new ArticleHistoricsDTO(key,value)));
		
		return lisArticleHistoricsDTOs;
	}


	
	//listadto.add(new ArticleHistoricsDTO(key,value))
}
