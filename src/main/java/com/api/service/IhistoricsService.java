package com.api.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.api.DTO.ArticleHistoricsDTO;
import com.api.DTO.ArticlesHistoricRequestDTO;
import com.api.DTO.ErrorDTO;
import com.api.entity.Articleshistorics;


public interface IhistoricsService {
	List<ArticleHistoricsDTO>ListArticleHistorics(Long articleTic,List<Integer> fieldTic);

	Page<Articleshistorics> TodasPaginacion(int pagina,int cantidad );
	
	ErrorDTO error();
	
	List<ArticleHistoricsDTO>ListArticleHistorics(ArticlesHistoricRequestDTO articlehis,int pagina,int cantidad );
}
