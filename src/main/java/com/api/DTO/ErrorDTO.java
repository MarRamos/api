package com.api.DTO;

import java.util.List;

public class ErrorDTO {
	private String message;
	private List<ErrorFieldDTO> fields;
	
	
	public ErrorDTO(String message, List<ErrorFieldDTO> fields) {
		super();
		this.message = message;
		this.fields = fields;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ErrorFieldDTO> getFields() {
		return fields;
	}
	public void setFields(List<ErrorFieldDTO> fields) {
		this.fields = fields;
	}
	

}
