package com.api.DTO;

import java.io.Serializable;
import java.util.List;

import com.api.entity.Articleshistorics;

public class ArticleHistoricsDTO {

	private Long parentHistoricTic;
	private List<Articleshistorics> articleHistoric;
	public ArticleHistoricsDTO(Long parentHistoricTic, List<Articleshistorics> articleHistoric) {
		this.parentHistoricTic = parentHistoricTic;
		this.articleHistoric = articleHistoric;
	}
	
	public Long getParentHistoricTic() {
		return parentHistoricTic;
	}

	public void setParentHistoricTic(Long parentHistoricTic) {
		this.parentHistoricTic = parentHistoricTic;
	}

	public List<Articleshistorics> getArticleHistoric() {
		return articleHistoric;
	}

	public void setArticleHistoric(List<Articleshistorics> articleHistoric) {
		this.articleHistoric = articleHistoric;
	}

	@Override
	public String toString() {
		return "ArticleHistoricsDTO [parentHistoricTic=" + parentHistoricTic + ", articleHistoric=" + articleHistoric
				+ "]";
	}
	
}
