package com.api.DTO;

import java.util.List;

public class ArticlesHistoricRequestDTO {
	private Long articleTic;
	private List<Integer> fieldTics;
	private List<Integer> colorTic;
	private List<Integer> sizeTic;
	private List<Integer> parentHistoricTic;
	private List<Integer> valueTic;
	private String value;
	private Boolean actualValue;
	private String creationDate;
	private String creationUser;
	public ArticlesHistoricRequestDTO() {
		
	}
	public Long getArticleTic() {
		return articleTic;
	}
	public void setArticleTic(Long articleTic) {
		this.articleTic = articleTic;
	}
	public List<Integer> getFieldTics() {
		return fieldTics;
	}
	public void setFieldTics(List<Integer> fieldTics) {
		this.fieldTics = fieldTics;
	}
	public List<Integer> getColorTic() {
		return colorTic;
	}
	public void setColorTic(List<Integer> colorTic) {
		this.colorTic = colorTic;
	}
	public List<Integer> getSizeTic() {
		return sizeTic;
	}
	public void setSizeTic(List<Integer> sizeTic) {
		this.sizeTic = sizeTic;
	}
	public List<Integer> getParentHistoricTic() {
		return parentHistoricTic;
	}
	public void setParentHistoricTic(List<Integer> parentHistoricTic) {
		this.parentHistoricTic = parentHistoricTic;
	}
	public List<Integer> getValueTic() {
		return valueTic;
	}
	public void setValueTic(List<Integer> valueTic) {
		this.valueTic = valueTic;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getActualValue() {
		return actualValue;
	}
	public void setActualValue(Boolean actualValue) {
		this.actualValue = actualValue;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}
	@Override
	public String toString() {
		return "ArticlesHistoricRequestDTO [articleTic=" + articleTic + ", fieldTics=" + fieldTics + ", colorTic="
				+ colorTic + ", sizeTic=" + sizeTic + ", parentHistoricTic=" + parentHistoricTic + ", valueTic="
				+ valueTic + ", value=" + value + ", actualValue=" + actualValue + ", creationDate=" + creationDate
				+ ", creationUser=" + creationUser + "]";
	}
	
	
	
}