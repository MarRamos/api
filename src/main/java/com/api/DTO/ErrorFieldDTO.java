package com.api.DTO;

public class ErrorFieldDTO {
private String field;
private String	errorFieldKey;
private String	description;
public String getField() {
	return field;
}
public void setField(String field) {
	this.field = field;
}
public String getErrorFieldKey() {
	return errorFieldKey;
}
public void setErrorFieldKey(String errorFieldKey) {
	this.errorFieldKey = errorFieldKey;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}

}
