package com.api.DTO;

public class ArticleHistoricDTO {
	private Long historicTic;
	private Long articleTic;
	private Integer fieldTic;
	private Integer colorTic;
	private Integer sizeTic;
	private Long valueTic;
	private String value;
	
	public ArticleHistoricDTO() {
		super();
	}
	public Long getHistoricTic() {
		return historicTic;
	}
	public void setHistoricTic(Long historicTic) {
		this.historicTic = historicTic;
	}
	public Long getArticleTic() {
		return articleTic;
	}
	public void setArticleTic(Long articleTic) {
		this.articleTic = articleTic;
	}
	public Integer getFieldTic() {
		return fieldTic;
	}
	public void setFieldTic(Integer fieldTic) {
		this.fieldTic = fieldTic;
	}
	public Integer getColorTic() {
		return colorTic;
	}
	public void setColorTic(Integer colorTic) {
		this.colorTic = colorTic;
	}
	public Integer getSizeTic() {
		return sizeTic;
	}
	public void setSizeTic(Integer sizeTic) {
		this.sizeTic = sizeTic;
	}
	public Long getValueTic() {
		return valueTic;
	}
	public void setValueTic(Long valueTic) {
		this.valueTic = valueTic;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getActualValue() {
		return actualValue;
	}
	public void setActualValue(Boolean actualValue) {
		this.actualValue = actualValue;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}
	private Boolean actualValue;
	private String creationDate;
	private String creationUser;
}
