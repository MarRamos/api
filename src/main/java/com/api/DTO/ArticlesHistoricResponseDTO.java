package com.api.DTO;

import java.util.List;

import com.api.entity.Articleshistorics;

public class ArticlesHistoricResponseDTO {
	private List<ArticleHistoricsDTO> articleHistorics;

	public ArticlesHistoricResponseDTO(List<ArticleHistoricsDTO> articleHistorics) {

		this.articleHistorics = articleHistorics;
	}

	public ArticlesHistoricResponseDTO() {

	}

	public List<ArticleHistoricsDTO> getArticleHistorics() {
		return articleHistorics;
	}

	public void setArticleHistorics(List<ArticleHistoricsDTO> articleHistorics) {
		this.articleHistorics = articleHistorics;
	}
	
	
		
}
