package com.api.controller;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.DTO.ArticlesHistoricRequestDTO;
import com.api.DTO.ArticlesHistoricResponseDTO;
import com.api.entity.Articleshistorics;
import com.api.repository.historicsRepository;
import com.api.service.IhistoricsService;

@RestController
@RequestMapping("/api/v2/articles")
public class ArticleHistoricsController<T> {
	@Autowired
	private historicsRepository historicsRepository;
	@Autowired
	private IhistoricsService ihistoricsService;

	

	@PostMapping("/historics")
	 public ResponseEntity<?> responseArticleHistoric(@RequestBody ArticlesHistoricRequestDTO articlesHistoricRequestDTO){
		try {
			if(articlesHistoricRequestDTO.getFieldTics().size()<=100 && !articlesHistoricRequestDTO.getArticleTic().equals("")) {
				ArticlesHistoricResponseDTO articlesHistoricResponseDTO=new ArticlesHistoricResponseDTO(ihistoricsService.ListArticleHistorics(articlesHistoricRequestDTO.getArticleTic(),articlesHistoricRequestDTO.getFieldTics()));
				return ResponseEntity.ok(articlesHistoricResponseDTO);
			}
			
			return ResponseEntity.status(400).body(ihistoricsService.error());
			
		} catch (Exception e) {
			
			return ResponseEntity.status(400).body(ihistoricsService.error());
		}
		
	 }
	
	@PostMapping("/pagart")
	public Object pagart(@RequestBody ArticlesHistoricRequestDTO articlesHistoricRequestDTO,@RequestParam("pagina")  int pagina, @RequestParam("cantidad")  int cantidad) {
		
		return ihistoricsService.ListArticleHistorics(articlesHistoricRequestDTO, pagina, cantidad);
		
	}
	
	
	@GetMapping("/paginacion")
	public Object paginacion(@RequestParam("pagina")  int pagina, @RequestParam("cantidad")  int cantidad) {
		
		return ihistoricsService.TodasPaginacion(pagina,cantidad);
	}


	@GetMapping("/generar")
	String generar() {
		for (int i = 0; i < 10; i++) {

			Articleshistorics objHistorics = new Articleshistorics();

			objHistorics.setHistoricTic(2969860 + i);
			objHistorics.setArticleTic(1765187);
			objHistorics.setFieldTic(100 + i);
			objHistorics.setColorTic(200 + 1);
			objHistorics.setSizeTic(1);
			objHistorics.setParentHistoricTic(296985308);
			objHistorics.setValueTic(500 + i);
			objHistorics.setValue("187");
			objHistorics.setActualValue(false);
			objHistorics.setCreationDate("2017-10-30T10:53:07.000+02:00");
			objHistorics.setCreationUser("CONTI/josemps");

			try {
				Thread.sleep(100);
				historicsRepository.save(objHistorics);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return "ingresado";
	}
}
