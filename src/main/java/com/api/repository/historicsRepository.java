package com.api.repository;

import org.springframework.stereotype.Repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


import com.api.entity.Articleshistorics;
@Repository
public interface historicsRepository extends JpaRepository<Articleshistorics,Long>{
	
	List<Articleshistorics> findByArticleTicAndFieldTicIn(Long articleTic,List<Integer> fieldTic);
	
	Page<Articleshistorics> findByArticleTicAndFieldTicIn(Long articleTic,List<Integer> fieldTic,Pageable pag);



}
