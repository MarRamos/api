package com.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "article_historics")
public class Articleshistorics {
	@Id
	private Long historicTic;
	private Long articleTic;
	private Integer fieldTic;
	private Integer colorTic;
	private Integer sizeTic;
	@JsonBackReference
	private Long parentHistoricTic;
	private Long valueTic;
	private String value;
	private Boolean actualValue;
	private String creationDate;
	private String creationUser;
	public Articleshistorics() {
		
	}
	public long getHistoricTic() {
		return historicTic;
	}
	public void setHistoricTic(long historicTic) {
		this.historicTic = historicTic;
	}
	public long getArticleTic() {
		return articleTic;
	}
	public void setArticleTic(long articleTic) {
		this.articleTic = articleTic;
	}
	public Integer getFieldTic() {
		return fieldTic;
	}
	public void setFieldTic(Integer fieldTic) {
		this.fieldTic = fieldTic;
	}
	public Integer getColorTic() {
		return colorTic;
	}
	public void setColorTic(Integer colorTic) {
		this.colorTic = colorTic;
	}
	public Integer getSizeTic() {
		return sizeTic;
	}
	public void setSizeTic(Integer sizeTic) {
		this.sizeTic = sizeTic;
	}
	public long getParentHistoricTic() {
		return parentHistoricTic;
	}
	public void setParentHistoricTic(long parentHistoricTic) {
		this.parentHistoricTic = parentHistoricTic;
	}
	public long getValueTic() {
		return valueTic;
	}
	public void setValueTic(long valueTic) {
		this.valueTic = valueTic;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isActualValue() {
		return actualValue;
	}
	public void setActualValue(boolean actualValue) {
		this.actualValue = actualValue;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}
	@Override
	public String toString() {
		return "historics [historicTic=" + historicTic + ", articleTic=" + articleTic + ", fieldTic=" + fieldTic
				+ ", colorTic=" + colorTic + ", sizeTic=" + sizeTic + ", parentHistoricTic=" + parentHistoricTic
				+ ", valueTic=" + valueTic + ", value=" + value + ", actualValue=" + actualValue + ", creationDate="
				+ creationDate + ", creationUser=" + creationUser + "]";
	}

}
